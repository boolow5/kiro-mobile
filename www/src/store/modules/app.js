const state = {
  drawer: false,
  layout: true,
  loading: {enable: false, msg: 'Initializing...'},
  listModeHome: true,
  defaultImage: 'not-found.png',
  filter_sidebar: false,
  is_demo_mode: false,
  no_image_default: '/dist/images/no-image-en.png',
  no_image_clear: '/dist/images/no-image-clear.png',
  maxFreeListing: 5
}

const getters = {
  drawer: (state) => state.drawer,
  layout: (state) => state.layout,
  app_loading: (state) => state.loading,
  listModeHome: (state) => state.listModeHome,
  defaultImage: (state) => state.defaultImage,
  filter_sidebar: (state) => state.filter_sidebar,
  is_demo_mode: (state) => state.is_demo_mode,
  noImageDefault: (state) => state.no_image_default,
  noImageClear: (state) => state.no_image_clear,
  maxFreeListing: (state) => state.maxFreeListing
}

const mutations = {
  DRAWER (state, value) {
    state.drawer = value === true
  },
  APP_LOADING (state, value) {
    console.log('APP_LOADING', value)
    if (typeof value === 'boolean') {
      state.loading.enable = value
    } else if (typeof value === 'string') {
      state.loading.msg = value
    } else if (typeof value === 'object' && value.hasOwnProperty('enable')) {
      state.loading = value
    }
  },
  IS_LIST_MODE_HOME (state, value) {
    state.listModeHome = value === true
  },
  SET_FILTER_SIDEBAR (state, value) {
    state.filter_sidebar = value === true
  },
  SET_DEMO_MODE (state, value) {
    state.is_demo_mode = value
  }
}

const actions = {
  CLEAR_ALL (context) {
    context.commit('CLEAR_USER_DATA')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
