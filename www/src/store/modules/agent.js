
const state = {
  agent: {},
  subscriptionTypes: [],
  mySubscription: {},
  quota: {used: 0, available: 5, total: 5}
}

const getters = {
  agent: (state) => {
    if (state.agent && state.agent.id) {
      return state.agent
    } else {
      let agentData = localStorage.getItem('agent')
      if (agentData !== null) {
        return JSON.parse(agentData).agent
      }
      return null
    }
  },
  subscriptionTypes: state => state.subscriptionTypes,
  mySubscription: state => state.mySubscription,
  isPaidSubscription: state => state.mySubscription && state.mySubscription.type === 'paid',
  quota: state => state.quota
}

const mutations = {
  SET_AGENT (state, payload) {
    state.agent = payload
    localStorage.setItem('agent', JSON.stringify(state))
  },
  SET_SUBSCRIPTION_TYPES (state, payload) {
    state.subscriptionTypes = payload
  },
  SET_MY_SUBSCRIPTION (state, payload) {
    state.mySubscription = payload
  },
  SET_LISTING_QUOTA (state, payload) {
    state.quota = payload
  }
}

const actions = {
  GET_LISTING_QUOTA ({commit}) {
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/listing-quota').then(resp => {
        console.log('GET_LISTING_QUOTA response', resp.data.quota)
        commit('SET_LISTING_QUOTA', resp.data.quota)
        resolve(resp.data)
      }).catch(err => {
        console.log('GET_SUBSCRIPTION_TYPES response', err)
        reject(err.body)
      })
    })
  },
  GET_SUBSCRIPTION_TYPES ({commit}) {
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/subscription-types').then(resp => {
        console.log('GET_SUBSCRIPTION_TYPES response', resp.data.subscription_types)
        commit('SET_SUBSCRIPTION_TYPES', resp.data.subscription_types)
        resolve(resp.data)
      }).catch(err => {
        console.log('GET_SUBSCRIPTION_TYPES response', err)
        reject(err.body)
      })
    })
  },
  GET_MY_SUBSCRIPTION ({commit}) {
    return new Promise((resolve, reject) => {
      let url = '/api/v1/my-subscription'
      if (!window.isLoggedIn()) {
        reject(new Error('Login first to access'))
        return
      }
      window.VueInstance.$http.get(url).then(resp => {
        console.log('GET_MY_SUBSCRIPTION response', resp.data.my_subscription)
        commit('SET_MY_SUBSCRIPTION', resp.data.my_subscription)
        resolve(resp.data)
      }).catch(err => {
        console.log('GET_MY_SUBSCRIPTION response', err)
        reject(err.body)
      })
    })
  },
  GET_AGENT ({commit, dispatch}, data) {
    console.log('GET_AGENT action', data)
    return new Promise((resolve, reject) => {
      if (!window.isLoggedIn()) {
        reject(new Error('Login first to access'))
        return
      }
      window.VueInstance.$http.get('/api/v1/agent', data).then(response => {
        console.log('GET_AGENT response', response)
        if (response && response.data && response.data.agent) {
          commit('SET_AGENT', response.data.agent)
          dispatch('GET_LISTING_QUOTA')
        }
        resolve(response.data)
      }).catch(err => {
        console.log('GET_AGENT error', err.data)
        reject(err.data)
      })
    })
  },
  GET_AGENT_BY_ID ({commit}, id) {
    console.log('GET_AGENT_BY_ID action', id)
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/get-agent/' + id).then(response => {
        console.log('GET_AGENT_BY_ID response', response)
        if (response && response.data && response.data.agent) {
          commit('SET_AGENT', response.data.agent)
        }
        resolve(response.data)
      }).catch(err => {
        console.log('GET_AGENT_BY_ID error', err.data)
        reject(err.data)
      })
    })
  },
  GET_REVIEWS_DATA (_, payload) {
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get(`/api/v1/ratings/${payload.target}?id=${payload.id}`).then(resp => {
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  },
  ADD_RATING (_, payload) {
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.post(`/api/v1/ratings/${payload.target}?id=${payload.id}&rate=${payload.rate}&comment=${payload.comment}`, payload).then(resp => {
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
