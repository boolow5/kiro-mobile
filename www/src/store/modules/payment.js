const state = {}
const getters = {}
const mutations = {}
const actions = {
  GENERATE_IPAY_KEY () {
    return new Promise((resolve, reject) => {
      if (!window.isLoggedIn()) {
        reject(new Error('Login first to access'))
        return
      }
      window.VueInstance.$http.get('/ipay/generate-key').then(resp => {
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  },
  COMPLETE_SUBSCRIPTION (_, data) {
    return new Promise((resolve, reject) => {
      if (!window.isLoggedIn()) {
        reject(new Error('Login first to access'))
        return
      }
      let method = ''
      if (data && data.method) {
        method = data.method
      }
      window.VueInstance.$http.post(`/api/v1/complete-subscription?method=${method}`, data).then(resp => {
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
