const state = {
  showHouseMapPopup: false,
  houseMapPopupData: {},
  houseMapPopupDataLoading: false,
  house_types: [],
  currencies: [],
  listing: [],
  most_viewd_listing: [],
  max_price: 0
}

const getters = {
  showHouseMapPopup: (state) => state.showHouseMapPopup,
  houseMapPopupData: (state) => state.houseMapPopupData,
  houseMapPopupDataLoading: (state) => state.houseMapPopupDataLoading,
  house_types: (state) => state.house_types,
  currencies: (state) => state.currencies,
  listing: (state) => state.listing,
  most_viewd_listing: (state) => state.most_viewd_listing,
  max_price: state => state.max_price
}

const mutations = {
  SET_SELECTED_HOUSE (state, payload) {
    state.houseMapPopupData = payload
  },
  SHOW_HOUSE_MAP_POPUP (state, payload) {
    console.log('SHOW_HOUSE_MAP_POPUP', payload)
    state.showHouseMapPopup = true
    // state.houseMapPopupData = payload
  },
  HIDE_HOUSE_MAP_POPUP (state) {
    state.showHouseMapPopup = false
  },
  SET_HOUSE_TYPES (state, payload) {
    state.house_types = payload
  },
  SET_CURRENCIES (state, payload) {
    state.currencies = payload
  },
  SET_MY_LISTING (state, payload) {
    state.listing = payload
  },
  SET_MY_MOST_VIEWED_LISTING (state, payload) {
    state.most_viewd_listing = payload
  },
  SET_MAX_PRICE (state, payload) {
    if (payload > state.max_price || payload === null) {
      state.max_price = payload
    }
  }
}

const actions = {
  GET_HOUSE_TYPES ({commit}) {
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/house-types').then(resp => {
        console.log('GET_HOUSE_TYPES response', resp.data.house_types)
        commit('SET_HOUSE_TYPES', resp.data.house_types)
        resolve(resp.data)
      }).catch(err => {
        console.log('GET_HOUSE_TYPES response', err)
        reject(err.body)
      })
    })
  },
  GET_MY_LISTING ({commit}, isMostViewed) {
    return new Promise((resolve, reject) => {
      let query = ''
      if (isMostViewed) {
        query = '?top_viewd=1'
      }
      window.VueInstance.$http.get('/api/v1/listing/my' + query).then(resp => {
        if (!window.isLoggedIn()) {
          reject(new Error('Login first to access'))
          return
        }
        console.log('GET_MY_LISTING response', resp.data.listing)
        if (!isMostViewed) {
          commit('SET_MY_LISTING', resp.data.listing)
        } else {
          commit('SET_MY_MOST_VIEWED_LISTING', resp.data.listing)
        }
        resolve(resp.data)
      }).catch(err => {
        console.log('GET_MY_LISTING response', err)
        reject(err.data)
      })
    })
  },
  ADD_NEW_LISTING (_, payload) {
    return new Promise((resolve, reject) => {
      if (!window.isLoggedIn()) {
        reject(new Error('Login first to access'))
        return
      }
      window.VueInstance.$http.post('/api/v1/house', payload).then(resp => {
        console.log('ADD_NEW_LISTING response', resp.data)
        resolve(resp.data)
      }).catch(err => {
        console.log('ADD_NEW_LISTING response', err)
        reject(err.data)
      })
    })
  },
  UPDATE_LISTING (_, payload) {
    return new Promise((resolve, reject) => {
      if (!window.isLoggedIn()) {
        reject(new Error('Login first to access'))
        return
      }
      window.VueInstance.$http.put('/api/v1/house', payload).then(resp => {
        console.log('UPDATE_LISTING response', resp.data)
        resolve(resp.data)
      }).catch(err => {
        console.log('UPDATE_LISTING response', err)
        reject(err.data)
      })
    })
  },
  DELETE_LISTING (_, payload) {
    return new Promise((resolve, reject) => {
      console.log('DELETE_LISTING', payload)
      if (!window.isLoggedIn()) {
        reject(new Error('Login first to access'))
        return
      }
      window.VueInstance.$http.delete('/api/v1/house?id=' + payload.id).then(resp => {
        console.log('DELETE_LISTING response', resp.data)
        resolve(resp.data)
      }).catch(err => {
        console.log('DELETE_LISTING response', err)
        reject(err.data)
      })
    })
  },
  GET_CURRENCIES ({commit}) {
    return new Promise((resolve, reject) => {
      window.VueInstance.$http.get('/api/v1/currency/all').then(resp => {
        console.log('GET_CURRENCIES response', resp.data.currencies)
        commit('SET_CURRENCIES', resp.data.currencies)
        resolve(resp.data)
      }).catch(err => {
        console.log('GET_CURRENCIES response', err)
        reject(err)
      })
    })
  },
  GET_HOUSE_BY_ID ({commit, state}, id) {
    console.log('GET_HOUSE_BY_ID action', id)
    return new Promise((resolve, reject) => {
      if (!id) {
        reject(new Error('Invalid house ID'))
        return
      }
      state.houseMapPopupDataLoading = true
      window.VueInstance.$http.get('/api/v1/house/' + id).then(response => {
        console.log('GET_HOUSE_BY_ID response', response)
        if (response && response.data && response.data.house) {
          commit('SET_SELECTED_HOUSE', response.data.house)
        }
        state.houseMapPopupDataLoading = false
        resolve(response.data)
      }).catch(err => {
        console.log('GET_HOUSE_BY_ID error', err.response)
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
