import 'babel-polyfill'
import Vue from 'vue'
import App from './App.vue'
import Cordova from './Cordova.js'
import i18n from 'vuex-i18n'
import vSelect from 'vue-select'
import VueRouter from 'vue-router'
import axios from 'axios'

import store from './store'
import router from './router'
import { sync } from 'vuex-router-sync'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import {dates, strings} from './mixins'
import SO from './translations/so'

import VuejsDialog from 'vuejs-dialog'
// import VuejsDialogMixin from 'vuejs-dialog/dist/vuejs-dialog-mixin.min.js' // only needed in custom components

// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css'

sync(store, router)

Vue.use(i18n.plugin, store)
Vue.i18n.add('so', SO)
Vue.i18n.set((localStorage.getItem('lang')) || 'en')

Vue.component('vue-select', vSelect)
Vue.mixin(dates)
Vue.mixin(strings)

Vue.use(Vuetify)
Vue.use(VuejsDialog)

// use the server url or local url
const DEBUG_MODE = true
console.log('ENV:', {DEBUG_MODE})

window.baseURL = DEBUG_MODE ? 'http://localhost:8082' : 'http://kiro-api.iweydi.com'
window.isLoggedIn = function () {
  let auth = window.localStorage.getItem('auth')
  if (auth !== null) {
    auth = JSON.parse(auth)
    return auth.jwt_token.length > 80 && auth.jwt_token && auth.jwt_token.split('.').length === 3
  }
  return false
}

window.clone = function (obj) {
  if (typeof obj === 'object') {
    return JSON.parse(JSON.stringify(obj))
  }
  return obj
}

Vue.use(VueRouter)
var http = axios.create({
  baseURL: window.baseURL,
  timeout: 15 * 1000,
  headers: {
    'Content-Type': 'application/json'
  }
})
http.interceptors.request.use(config => {
  let auth = window.localStorage.getItem('auth')
  if (auth !== null) {
    auth = JSON.parse(auth)
    config.headers['Authorization'] = auth.jwt_token
  }
  return config
}, err => {
  console.log('axiso error', err)
})
Vue.prototype.$http = http

export const EventBus = new Vue()

// Load Vue instance
export default new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
  mounted () {
    Cordova.initialize()
  },
  created () {
    window.VueInstance = this
  }
})
