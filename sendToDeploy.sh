#!/bin/sh

# variables
PKGNAME=app.tar.gz
HOST_NAME=45.79.67.142
USER_NAME=mahdi
BASE_DIR=$PWD
DESTINATION_DIR=/var/www/kiro/app
SOURCE_DIR=$BASE_DIR/platforms/browser/www

# initialize colors
red=`tput setaf 1`
green=`tput setaf 2`
cyan=`tput setaf 6`
reset=`tput sgr0`


echo "${green}Sending files to server: ${HOST_NAME} with user: ${USER_NAME}"
echo "${reset}project directory: ${BASE_DIR}"
echo "source directory: ${SOURCE_DIR}"
echo "server's destination directory: ${DESTINATION_DIR}"
echo ""
echo "${green}compressing files..."
cd $SOURCE_DIR
tar -zcf $BASE_DIR/${PKGNAME} .
cd $BASE_DIR
echo "${cyan}"
echo "scp -i  ~/.ssh/id_rsa $PKGNAME $USER_NAME@$HOST_NAME:$DESTINATION_DIR"
echo "${cyan}"

# rsync -auv -e '/usr/bin/ssh -f -v -i' --progress $SOURCE_DIR/* $USER_NAME@$HOST_NAME:$DESTINATION_DIR
scp -i  ~/.ssh/id_rsa $PKGNAME $USER_NAME@$HOST_NAME:/tmp

echo "${green}"

ssh -i  ~/.ssh/id_rsa -t $USER_NAME@$HOST_NAME "
  sudo rm -r $DESTINATION_DIR/*
  sudo tar -xzf /tmp/$PKGNAME -C $DESTINATION_DIR/;
  echo ${green}Moved all files to: $DESTINATION_DIR;
  # sudo cp $DESTINATION_DIR/dist/service-worker.js $DESTINATION_DIR;
  echo Done moving `ls -1a | wc -l` files.
";
echo "${reset}"